#! /bin/bash

VALHEIM_DIR="${HOME}/valheim"
VALHEIM_SERVER_DIR=$VALHEIM_DIR/valheim/server

[ ! -d $VALHEIM_SERVER_DIR ] || mkdir -p $VALHEIM_SERVER_DIR

cp "$PWD/docker-compose.yml" $VALHEIM_DIR


# add backup cronjob if not exists
grep '/root/valheim-host/scripts/backup-worlds.sh' /var/spool/cron/crontabs/root || \
  (crontab -l 2>/dev/null; echo "30 * * * * /root/valheim-host/scripts/backup-worlds.sh") | crontab -

## valheim plus (BepInEx comes with this => no need to install manually)
curl -s https://api.github.com/repos/valheimPlus/ValheimPlus/releases/latest \
| grep "/UnixServer.zip" \
| cut -d : -f 2,3 \
| tr -d \" \
| wget -qi -
unzip -oq UnixServer.zip -d $VALHEIM_SERVER_DIR
cp "$PWD/configs/valheim_plus.cfg" $VALHEIM_SERVER_DIR/BepInEx/config

# valheim longship
# curl -s https://api.github.com/repos/AlexMog/Longship/releases/latest \
# | grep "/distdll.zip" \
# | cut -d : -f 2,3 \
# | tr -d \" \
# | wget -qi -
# unzip -oq distdll.zip -d $VALHEIM_SERVER_DIR/BepInEx/plugins
# cp "$PWD/configs/gg.mog.valheim.longship.cfg" $VALHEIM_SERVER_DIR/BepInEx/config

