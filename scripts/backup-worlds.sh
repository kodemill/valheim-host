#! /bin/bash

BACKUP_DIR="/root/backup"
VALHEIM_DIR="/root/valheim"
WORLDS_DIR="${VALHEIM_DIR}/valheim/saves/worlds/"
DATE=$(date +%m-%d-%H-%M)

# create backup dir if not exists
[ ! -d $BACKUP_DIR ] || mkdir -p $BACKUP_DIR

# make backup of the worlds dir into backup dir (tar,gz)
tar -zcvpf $BACKUP_DIR/worlds-$DATE.tar.gz $WORLDS_DIR

# Delete files older than 3 days #
find $BACKUP_DIR/* -mtime +3 -exec rm {} \;
